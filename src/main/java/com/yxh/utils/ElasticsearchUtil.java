package com.yxh.utils;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.yxh.annotions.DocumentIndex;
import com.yxh.annotions.HighLight;
import com.yxh.annotions.IdIndex;
import com.yxh.annotions.PropertiesIndex;
import com.yxh.domain.HighLightDTO;
import com.yxh.domain.enums.JdkDataTypeEnum;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author：易小航
 * @name：ElasticsearchUtil
 * @Date：2024/9/14 14:43
 */
@Log4j2
@Component
public class ElasticsearchUtil {

    /**
     * 判断索引是否存在
     * @param client 客户端
     * @param indexName 索引名
     * @return true=>已存在 false=>不存在
     * */
    public boolean existsIndex(RestHighLevelClient client,String indexName){
        GetIndexRequest request = new GetIndexRequest(new String[]{indexName});
        try {
            return client.indices().exists(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建ID
     * */
    private String getObjectId(Object data){
        String idValue = null;
        try {
            Field[] fields = data.getClass().getDeclaredFields();
            for (Field field : fields) {
                IdIndex annotation = field.getAnnotation(IdIndex.class);
                if (null != annotation){
                    field.setAccessible(true);
                    idValue = field.get(data).toString();
                    break;
                }
            }
        } catch (Exception e){
            log.error("报错信息:{}",e);
        }
        return idValue;
    }

    /**
     * 创建索引
     * @param client 客户端
     * @param data 类名
     * */
    public String createIndex(RestHighLevelClient client,Object data) throws Exception {
        //根据实体注解取索引名字
        DocumentIndex annotation = data.getClass().getAnnotation(DocumentIndex.class);
        String indexName = annotation.indexName();

        //索引已经存在，不需要创建索引
        if (existsIndex(client,indexName)) {
            return indexName;
        }

        //1.创建索引请求
        CreateIndexRequest request = new CreateIndexRequest(indexName);

        //创建基础配置
        Settings.Builder builder = Settings.builder().put("index.max_result_window", annotation.maxSize());//10亿数据
        builder.put("index.number_of_shards", annotation.shards()) // 分片数量
                .put("index.number_of_replicas", annotation.replicas()); // 副本数量
        request.settings(builder);//索引文档基础配置

        //mapping结构
        JSONObject mapping = new JSONObject();
        JSONObject props = new JSONObject();
        mapping.put("properties", props);

        Class<?> aClass = data.getClass();
        //取对象所有私有属性
        Field[] declaredFields = aClass.getDeclaredFields();
        for (Field field : declaredFields) {
            Class<?> type = field.getType();
            String name = field.getName();
            JSONObject prop = new JSONObject();
            PropertiesIndex propIndex = field.getAnnotation(PropertiesIndex.class);
            if (propIndex != null) {//自定义属性各种配置
                if (propIndex.name() != null && !propIndex.name().isEmpty()) {
                    name = propIndex.name();
                }
                props.put(name,prop);//通过注解可以指定索引字段名称
                prop.put("type", propIndex.type());
                prop.put("index", true);//默认true
                if ("text".equals(propIndex.type())){
                    prop.put("analyzer",propIndex.analyzer());//"analyzer": "ik_max_word",
                    prop.put("search_analyzer",propIndex.searchAnalyzer());//"search_analyzer": "ik_smart"
                }
                if (!propIndex.index()) { //设置非索引
                    prop.put("index", false);
                }
            } else { //默认处理
                props.put(name,prop);
                JdkDataTypeEnum byType = JdkDataTypeEnum.getByType(type.getSimpleName());
                switch (byType){
                    case STRING:
                        prop.put("type","keyword");
                        break;
                    case DATE:
                        prop.put("type","date");
                        prop.put("format","yyyy-MM-dd HH:mm:ss");//"format": "yyyy-MM-dd HH:mm:ss"
                        break;
                    case INTEGER:
                        prop.put("type","integer");
                        break;
                    case LONG:
                        prop.put("type","long");
                        break;
                    default:
                        prop.put("type","text");
                        prop.put("analyzer","ik_smart");//"analyzer": "ik_max_word",
                        prop.put("search_analyzer","ik_smart");//"search_analyzer": "ik_smart"
                        break;
                }
            }

        }

        String jsonString = mapping.toJSONString();
        log.info("jsonString: " + jsonString);
        request.mapping(jsonString, XContentType.JSON);

        //2.执行客户端请求
        client.indices().create(request, RequestOptions.DEFAULT);
        return indexName;
    }

    /**
     * 简单版创建
     * */
    public boolean create(RestHighLevelClient client,String indexName){
        //判断indexName是否存在
        if (existsIndex(client,indexName)) {
            log.error("{}索引已存在",indexName);
            return false;//已存在
        }
        CreateIndexRequest request = new CreateIndexRequest(indexName);
        try {
            return client.indices().create(request, RequestOptions.DEFAULT).isAcknowledged();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 删除索引
     * @param client 客户端
     * @param indexName 索引名
     * */
    public boolean deleteIndex(RestHighLevelClient client,String indexName){
        //判断indexName是否存在
        if (!existsIndex(client,indexName)) {
            log.error("{}索引不存在",indexName);
            return false;//不存在
        }
        DeleteIndexRequest request = new DeleteIndexRequest(indexName);
        request.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);
        try {
            return client.indices().delete(request, RequestOptions.DEFAULT).isAcknowledged();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 添加
     * */
    public boolean insert(Object data,String indexName,RestHighLevelClient client){
        try {
            IndexRequest request = new IndexRequest(indexName)
                    .id(getObjectId(data))
                    .source(JSON.toJSONString(data), XContentType.JSON);
            client.index(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    /**
     * 批量添加
     * */
    public boolean insertBatch(List<Object> list, String indexName, RestHighLevelClient client){
        try {
            BulkRequest request = new BulkRequest();
            list.forEach(data -> {
                request.add(new IndexRequest(indexName)
                        .id(getObjectId(data))
                        .source(JSON.toJSONString(data), XContentType.JSON));
            });
            client.bulk(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    /**
     * 删除
     * */
    public boolean delete(Object data, String indexName, RestHighLevelClient client){
        try {
            DeleteRequest request = new DeleteRequest(indexName, getObjectId(data));
            client.delete(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    /**
     * 批量删除
     * */
    public boolean deleteBatch(List<Object> list, String indexName, RestHighLevelClient client){
        try {
            BulkRequest request = new BulkRequest();
            list.forEach(data -> {
                request.add(new DeleteRequest(indexName,getObjectId(data)));
            });
            client.bulk(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    /**
     * 修改
     * */
    public boolean update(Object data, String indexName, RestHighLevelClient client){
        try {
            UpdateRequest request = new UpdateRequest(indexName, getObjectId(data))
                    .doc(JSON.toJSONString(data), XContentType.JSON);
            client.update(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    /**
     * 批量修改
     * */
    public boolean updateBatch(List<Object> list, String indexName, RestHighLevelClient client){
        try {
            BulkRequest request = new BulkRequest();
            list.forEach(data -> {
                request.add(new UpdateRequest(indexName, getObjectId(data))
                        .doc(JSON.toJSONString(data), XContentType.JSON));
            });

            client.bulk(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    /**
     * 根据Id查询
     * */
    public Object findById(String id,String indexName,RestHighLevelClient client){
        GetRequest request = new GetRequest(indexName, id);
        try {
            return client.get(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 查询所有
     * */
    public List<Object> selectAll(String indexName,RestHighLevelClient client){
        List<Object> list = new ArrayList<>();
        try {
            SearchRequest request = new SearchRequest(indexName);
            SearchResponse search = client.search(request, RequestOptions.DEFAULT);
            for (SearchHit hit : search.getHits().getHits()) {
                String source = hit.getSourceAsString();
                list.add(JSON.parseObject(source,Object.class));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    /**
     * 读取@HighLight映射字段
     * */
    private HighLightDTO readHighLight(Class<?> data) throws IllegalAccessException {
        Field[] fields = data.getDeclaredFields();
        String name = null;
        String preTags = null;
        String postTags = null;
        String mapping = null;
        for (Field field : fields) {
            HighLight annotation = field.getAnnotation(HighLight.class);//映射@highLight值
            if (annotation!=null){
                field.setAccessible(true);
                name = field.getName();//字段名
                preTags = annotation.preTag();//自定义标签前缀
                postTags = annotation.postTag();//自定义标签后缀
                mapping = annotation.mappingField();//映射字段
            }
        }
        return new HighLightDTO(name,preTags,postTags,mapping);
    }

    /**
     * 给高亮值赋值
     */
    private void assignment(Object data, String fieldName, String newValue) throws IllegalAccessException, NoSuchFieldException {
        Field field = data.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(data, convertValue(field.getType(), newValue));
    }

    /**
     * 转类型
     */
    private Object convertValue(Class<?> fieldType, String value) {
        JdkDataTypeEnum byType = JdkDataTypeEnum.getByType(String.valueOf(fieldType));
        switch (byType){
            case INTEGER:
                return Integer.parseInt(value);
            case LONG:
                return Long.parseLong(value);
            case BOOLEAN:
                return Boolean.parseBoolean(value);
            default:
                return value;
        }
    }

    /**
     * 高亮查询
     * @param request 参数
     * @param data 泛型
     * @param indexName 索引名
     * @param client 连接
     * */
    public List<Object> queryHighLight(String request, Class<?> data, String indexName, RestHighLevelClient client) throws IllegalAccessException {
        List<Object> list = new ArrayList<>();
        //获取list的泛型
        HighLightDTO highLightDTO = readHighLight(data);
        try {
            //创建请求索引
            SearchRequest searchRequest = new SearchRequest(indexName);
            //创建构造器
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            //创建条件拼接
            BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
            //查询用户名 模糊查询
            if (null != request){
                boolQueryBuilder.must(QueryBuilders.wildcardQuery(highLightDTO.getName(),"*"+request+"*"));
            }
            //条件存放构造器
            searchSourceBuilder.query(boolQueryBuilder);
            //高亮
            searchSourceBuilder.highlighter(new HighlightBuilder().field(highLightDTO.getName()).preTags(highLightDTO.getPreTag()).postTags(highLightDTO.getPostTag()));
            searchRequest.source(searchSourceBuilder);
            SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = search.getHits();
            for (SearchHit hit : hits.getHits()) {
                String sourceAsString = hit.getSourceAsString();
                Object object = JSONObject.parseObject(sourceAsString, data);
                //获取高亮集合
                Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                if (null!=highlightFields){
                    HighlightField obj = highlightFields.get(highLightDTO.getName());
                    if (null != obj){
                        String str = "";
                        for (Text fragment : obj.getFragments()) {
                            str += fragment;
                        }
                        //将字段赋值
                        assignment(object, highLightDTO.getMapping(), str);
                    }
                }
                //将存储的结果存入list集合
                list.add(object);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return list;
    }

}
