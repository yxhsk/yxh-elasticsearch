package com.yxh.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @Author：易小航
 * @name：GenericTypeUtil
 * @Date：2024/10/14 19:44
 */
public class GenericTypeUtil {
    /**
     * 获取泛型类型的名称
     * @param <T> 泛型类型
     * @param instance 泛型类的实例
     * @return 泛型类型的名称
     */
    public static <T> Object getGenericTypeName(T instance) {
        // 获取实例的类
        Class<?> clazz = instance.getClass();

        // 获取类的泛型父类信息
        Type genericSuperclass = clazz.getGenericSuperclass();

        if (genericSuperclass instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
            // 获取实际的泛型参数类型
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();

            // 假设 YxhEsServiceImpl 只接受一个泛型参数
            if (actualTypeArguments.length > 0) {
                return actualTypeArguments[0].getTypeName();
            }
        }
        return null;
    }

    /**
     * 获取类上的泛型T
     *
     * @param o     接口
     * @param index 泛型索引
     */
    public static Class<?> getClassT(Object o, int index) {
        Type type = o.getClass().getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type actType = parameterizedType.getActualTypeArguments()[index];
            return checkType(actType, index);
        } else {
            String className = type == null ? "null" : type.getClass().getName();
            throw new IllegalArgumentException("Expected a Class, ParameterizedType"
                    + ", but <" + type + "> is of type " + className);
        }
    }

    private static Class<?> checkType(Type type, int index) {
        if (type instanceof Class<?>) {
            return (Class<?>) type;
        } else if (type instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) type;
            Type t = pt.getActualTypeArguments()[index];
            return checkType(t, index);
        } else {
            String className = type == null ? "null" : type.getClass().getName();
            throw new IllegalArgumentException("Expected a Class, ParameterizedType"
                    + ", but <" + type + "> is of type " + className);
        }
    }
}
