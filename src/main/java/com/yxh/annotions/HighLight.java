package com.yxh.annotions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author：易小航
 * @name：LineHeight
 * @Date：2024/10/10 19:52
 * 高亮
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface HighLight {

    /**
     * 映射高亮字段
     * */
    String mappingField() default "";

    /**
     * 高亮字段内容长度
     * */
    int fragmentSize() default 100;

    /**
     * 前标签
     * */
    String preTag() default "<em>";

    /**
     * 后标签
     * */
    String postTag() default "</em>";

}
