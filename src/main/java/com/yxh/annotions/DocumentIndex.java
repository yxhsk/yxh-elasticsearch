package com.yxh.annotions;

import java.lang.annotation.*;

/**
 * @Author：易小航
 * @name：DocumetIndex
 * @Date：2024/9/14 15:01
 */
@Documented
@Target({ElementType.TYPE,ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DocumentIndex {

    String indexName() default ""; //索引名
    int maxSize() default 100000000; //最大数据量

    int shards() default 3; //分片

    int replicas() default 1; //副本(备份)
}
