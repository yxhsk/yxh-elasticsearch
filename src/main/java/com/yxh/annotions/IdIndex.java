package com.yxh.annotions;

import com.yxh.domain.enums.IdType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author：易小航
 * @name：IdIndex
 * @Date：2024/9/14 15:02
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IdIndex {

}
