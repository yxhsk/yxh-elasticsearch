package com.yxh.annotions;

import javax.lang.model.type.ReferenceType;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author：易小航
 * @name：PropertiesIndex
 * @Date：2024/9/14 15:38
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PropertiesIndex {

    String name() default ""; //类型名称

    String type() default "keyword"; //类型

    String analyzer() default "ik_smart"; //分片

    boolean index() default true; //是否建立索引

    String searchAnalyzer() default "ik_smart"; //查询分片

    boolean ignore() default true; //是否创建索引

}
