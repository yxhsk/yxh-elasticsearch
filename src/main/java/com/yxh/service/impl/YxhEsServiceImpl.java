package com.yxh.service.impl;

import com.yxh.annotions.DocumentIndex;
import com.yxh.service.YxhEsService;
import com.yxh.utils.ElasticsearchUtil;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @Author：易小航
 * @name：YxhEsServiceImpl
 * @Date：2024/9/15 11:15
 */
public class YxhEsServiceImpl<T> implements YxhEsService<T> {

    @Resource
    private ElasticsearchUtil elasticsearchUtil;

    @Autowired
    private RestHighLevelClient client;


    @Override
    public String createIndex(T data) {
        String index = null;
        try {
            index = elasticsearchUtil.createIndex(client, data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return index;
    }

    @Override
    public boolean deleteIndex(T data) {
        return elasticsearchUtil.deleteIndex(client,getIndexName());
    }

    @Override
    public boolean existsIndex(T data) {
        return elasticsearchUtil.existsIndex(client,getIndexName());
    }

    @Override
    public boolean insert(T data) {
        if (!existsIndex(data)) createIndex(data);
        return elasticsearchUtil.insert(data, getIndexName(), client);
    }

    @Override
    public boolean insertBatch(List<T> list) {
        if (!existsIndex((T) list)) createIndex((T) list);
        return elasticsearchUtil.insertBatch((List<Object>) list, getIndexName(), client);
    }

    @Override
    public boolean update(T data) {
        if (!existsIndex(data)) createIndex(data);
        return elasticsearchUtil.update(data,getIndexName(),client);
    }

    @Override
    public boolean updateBatch(List<T> list) {
        if (!existsIndex((T) list)) createIndex((T) list);
        return elasticsearchUtil.updateBatch((List<Object>) list,getIndexName(),client);
    }

    @Override
    public boolean delete(T data) {
        if (!existsIndex(data)) createIndex(data);
        return elasticsearchUtil.delete(data,getIndexName(),client);
    }

    @Override
    public boolean deleteBatch(List<T> list) {
        if (!existsIndex((T) list)) createIndex((T) list);
        return elasticsearchUtil.deleteBatch((List<Object>) list,getIndexName(),client);
    }

    @Override
    public T findById(String id) {
        return (T) elasticsearchUtil.findById(id, getIndexName(), client);
    }

    @Override
    public List<T> findAll() {
        return (List<T>) elasticsearchUtil.selectAll(getIndexName(),client);
    }

    @Override
    public List<T> highListList(String mapping) {
        //if (null == mapping) throw new RuntimeException();
        try {
            return (List<T>) elasticsearchUtil.queryHighLight(mapping,deSerializable(),getIndexName(),client);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取索引名
     * */
    @SuppressWarnings("unchecked")
    private String getIndexName(){
        Class<T> type = null;
        Type genericSuperclass = getClass().getGenericSuperclass();
        if (genericSuperclass instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) genericSuperclass;
            Type t = pt.getActualTypeArguments()[0];
            if (t instanceof Class<?>) {
                Class<?> clazz = (Class<?>) t;
                System.out.println("Type of T is: " + clazz.getName());
                type = (Class<T>) clazz;
            }
        }
        assert type != null;
        DocumentIndex annotation = type.getAnnotation(DocumentIndex.class);
        if (null == annotation){
            throw new RuntimeException("请先标识@DocumentIndex注解");
        }
        return annotation.indexName();
    }

    /**
     * 获取泛型
     * */
//    private Object getObjectClass(){
//        YxhEsServiceImpl<T> myClassInstance = new YxhEsServiceImpl<>();
//        // 获取类的泛型类型信息
//        String aClass = null;
//        Type genericSuperclass = myClassInstance.getClass().getGenericSuperclass();
//        if (genericSuperclass instanceof ParameterizedType) {
//            ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
//            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
//            for (Type type : actualTypeArguments) {
//                aClass = type.getTypeName();
//            }
//        }
//        return aClass;
//    }

    public static <T> Class<T> getTypeParameterClass(Class<? extends YxhEsServiceImpl<?>> clazz) {
        // 获取父类的泛型类型信息（这里的父类是YxhEsServiceImpl<T>，但由于clazz是YxhEsServiceImpl的子类（或自身），所以这是有效的）
        Type genericSuperclass = clazz.getGenericSuperclass();

        if (genericSuperclass instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
            // 获取实际的泛型参数类型
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();

            // 假设YxhEsServiceImpl只接受一个泛型参数，并且我们想要获取这个参数的类型
            if (actualTypeArguments.length > 0) {
                // 将Type转换为Class（这里可能会抛出ClassCastException，如果实际类型参数不是Class类型的话）
                // 注意：如果泛型参数是泛型类型（比如List<String>），这里将只会返回List.class，而不是具体的参数化类型
                return (Class<T>) actualTypeArguments[0];
            }
        }
        throw new IllegalArgumentException("Class " + clazz.getName() + " is not parameterized with at least one type argument");
    }


    /**
     * 获取泛型
     */
    private Class<T> deSerializable() {
        Type type = getClass().getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            System.out.println(parameterizedType.getActualTypeArguments()[0]);
            return (Class<T>) parameterizedType.getActualTypeArguments()[0];
        }
        throw new RuntimeException();
    }
}
