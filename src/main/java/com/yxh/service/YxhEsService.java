package com.yxh.service;

import java.util.List;

/**
 * @Author：易小航
 * @name：YxhEs
 * @Date：2024/9/15 11:09
 */
public interface YxhEsService<T> {

    /**
     * 创建索引
     * */
    String createIndex(T data);

    /**
     * 删除索引
     * */
    boolean deleteIndex(T data);

    /**
     * 判断索引是否存在
     * */
    boolean existsIndex(T data);

    /**
     * 添加
     * */
    boolean insert(T data);

    /**
     * 批量添加
     * */
    boolean insertBatch(List<T> list);

    /**
     * 修改
     * */
    boolean update(T data);

    /**
     * 批量修改
     * */
    boolean updateBatch(List<T> list);

    /**
     * 删除
     * */
    boolean delete(T data);

    /**
     * 批量删除
     * */
    boolean deleteBatch(List<T> list);

    /**
     * 根据Id查询
     * */
    T findById(String id);

    /**
     * 查询所有
     * */
    List<T> findAll();

    /**
     * 高亮查询
     * */
    List<T> highListList(String mapping);

}
