package com.yxh.domain.enums;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;

/**
 * @Author：易小航
 * @name：JdkDataType
 * @Date：2024/9/14 15:41
 */
public enum JdkDataTypeEnum {

    BYTE("byte"),
    SHORT("short"),
    INT("int"),
    INTEGER("Integer"),
    LONG("long"),
    FLOAT("float"),
    DOUBLE("double"),
    BIG_DECIMAL("BigDecimal"),
    BOOLEAN("boolean"),
    CHAR("char"),
    STRING("String"),
    DATE("Date"),
    LOCAL_DATE("LocalDate"),
    LOCAL_DATE_TIME("LocalDateTime"),
    LIST("List");

    public String type;

    public static JdkDataTypeEnum getByType(String typeName) {
        return (JdkDataTypeEnum) Arrays.stream(values()).filter((v) -> {
            return Objects.equals(v.type, typeName);
        }).findFirst().orElse(STRING);
    }

    private JdkDataTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }
}
