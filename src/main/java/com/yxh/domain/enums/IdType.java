package com.yxh.domain.enums;

/**
 * @Author：易小航
 * @name：IdType
 * @Date：2024/9/14 15:34
 */
public enum IdType {
    UUID,//随机值
    NONE,//空值
    CUSTOMIZE;//自定义
    private IdType(){}
}
