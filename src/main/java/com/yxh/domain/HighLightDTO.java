package com.yxh.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author：易小航
 * @name：HighLightDTO
 * @Date：2024/10/14 10:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HighLightDTO {
    private String name;
    private String preTag;
    private String postTag;
    private String mapping;
}
