package com.yxh;

import com.alibaba.fastjson2.JSONObject;
import com.yxh.annotions.DocumentIndex;
import com.yxh.annotions.IdIndex;
import com.yxh.annotions.PropertiesIndex;
import com.yxh.domain.enums.JdkDataTypeEnum;
import com.yxh.service.impl.YxhEsServiceImpl;
import com.yxh.utils.ElasticsearchUtil;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.yxh.service.impl.YxhEsServiceImpl.getTypeParameterClass;

@SpringBootTest
class YxhElasticsearchApplicationTests {

    @Autowired
    private ElasticsearchUtil elasticsearchUtil;

    @Autowired
    private RestHighLevelClient client;

    @Test
    void createIndex() throws Exception {
        elasticsearchUtil.createIndex(client,new User());
    }

    @Test
    void test() throws InstantiationException, IllegalAccessException {
        User data = new User();
        DocumentIndex annotation = data.getClass().getAnnotation(DocumentIndex.class);
        String indexName = annotation.indexName();
        System.out.println(indexName);
        JSONObject mapping = new JSONObject();
        JSONObject props = new JSONObject();
        mapping.put("properties", props);
        System.out.println(mapping);
        Class<?> aClass = data.getClass();
        System.out.println(aClass);
        //取对象所有私有属性

        Field[] declaredFields = aClass.getDeclaredFields();
        for (Field field : declaredFields) {
            Class<?> type = field.getType(); //字段类型
            String name = field.getName(); //字段名
            JSONObject prop = new JSONObject();
            PropertiesIndex propertiesIndex = field.getAnnotation(PropertiesIndex.class);
            if (null != propertiesIndex){
                String type1 = propertiesIndex.type();
                System.out.println(type1);
                System.out.println(type.getSimpleName());
            }else{
                props.put(name,prop);
                System.out.println("type:"+type.getTypeName());

//                if (type.newInstance() instanceof String){
//                    prop.put("type","keyword");
//                } else if (type.newInstance() instanceof Date){
//                    prop.put("type","date");
//                    prop.put("format","yyyy-MM-dd HH:mm:ss");//"format": "yyyy-MM-dd HH:mm:ss"
//                } else if (type.newInstance() instanceof Integer){
//                    prop.put("type","integer");
//                } else if (type.newInstance() instanceof Long){
//                    prop.put("type","long");
//                } else {
//                    prop.put("type","text");
//                    prop.put("analyzer","ik_smart");//"analyzer": "ik_max_word",
//                    prop.put("search_analyzer","ik_smart");//"search_analyzer": "ik_smart"
//                }
            }
        }

//        for (Field field : declaredFields) {
//            Class<?> type = field.getType();
//            String name = field.getName();
//            JSONObject prop = new JSONObject();
//            PropertiesIndex propIndex = field.getAnnotation(PropertiesIndex.class);
//            if (propIndex != null) {//自定义属性各种配置
//                if (propIndex.name() != null && !propIndex.name().isEmpty()) {
//                    name = propIndex.name();
//                }
//                props.put(name,prop);//通过注解可以指定索引字段名称
//                prop.put("type", propIndex.type());
//                prop.put("index", true);//默认true
//                if ("text".equals(propIndex.type())){
//                    prop.put("analyzer",propIndex.analyzer());//"analyzer": "ik_max_word",
//                    prop.put("search_analyzer",propIndex.searchAnalyzer());//"search_analyzer": "ik_smart"
//                }
//                if (!propIndex.index()) { //设置非索引
//                    prop.put("index", false);
//                }
//            } else { //默认处理
//                props.put(name,prop);
//                if (type.newInstance() instanceof String){
//                    prop.put("type","keyword");
//                } else if (type.newInstance() instanceof Date){
//                    prop.put("type","date");
//                    prop.put("format","yyyy-MM-dd HH:mm:ss");//"format": "yyyy-MM-dd HH:mm:ss"
//                } else if (type.newInstance() instanceof Integer){
//                    prop.put("type","integer");
//                } else if (type.newInstance() instanceof Long){
//                    prop.put("type","long");
//                } else {
//                    prop.put("type","text");
//                    prop.put("analyzer","ik_smart");//"analyzer": "ik_max_word",
//                    prop.put("search_analyzer","ik_smart");//"search_analyzer": "ik_smart"
//                }
//            }
//
//        }
//
//        String jsonString = mapping.toJSONString();

    }

    @Test
    void deleteIndex(){
        elasticsearchUtil.deleteIndex(client,"user");
    }

    @Test
    void selectAll() {
        List<Object> user = elasticsearchUtil.selectAll("user", client);
        user.forEach(System.out::println);
    }

    @Test
    void findById(){
        Object user = elasticsearchUtil.findById("7", "user", client);
        System.out.println(user);
    }

    @Test
    void insert(){
        User user = new User(6,"王金",3,"");
        elasticsearchUtil.insert(user, "user", client);
    }


    @Test
    void updateBatch(){
        List<Object> list = new ArrayList<>();
        list.add(new User(7,"nw",5,""));
        list.add(new User(8,"ww",5,""));
        list.add(new User(9,"dz",5,""));
        elasticsearchUtil.updateBatch(list, "user", client);
    }

    @Test
    void insertBatch(){
        List<Object> list = new ArrayList<>();
        list.add(new User(7,"牛无",5,""));
        list.add(new User(8,"弯弯",5,""));
        list.add(new User(9,"大壮",5,""));
        elasticsearchUtil.insertBatch(list, "user", client);
    }

    @Test
    void delete(){
        User user = new User();
        user.setUserId(1);
        elasticsearchUtil.delete("1", "user", client);
    }

    @Test
    void deleteBatch(){
        List<Object> list = new ArrayList<>();
        list.add(new User(7,"牛无",5,""));
        list.add(new User(8,"弯弯",5,""));
        list.add(new User(9,"大壮",5,""));
        elasticsearchUtil.deleteBatch(list, "user", client);
    }

    @Test
    void update(){
        User user = new User();
        user.setUserId(6);
        user.setUserName("王金");
        user.setClazzId(6);
        elasticsearchUtil.update(user, "user", client);
    }

    @Test
    void getObjectId() throws Exception {
        User data = new User(6,"王金",3,"");
        String idValue = null;
        Field[] fields = data.getClass().getDeclaredFields();
        for (Field field : fields) {
            IdIndex annotation = field.getAnnotation(IdIndex.class);
            if (null != annotation){
                field.setAccessible(true);
                idValue = field.get(data).toString();
                break;
            }
        }
        System.out.println(idValue);
    }


    @Test
    void demo02(){
        Type genericSuperclass = getClass().getGenericSuperclass();

        if (genericSuperclass instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();

            if (actualTypeArguments != null && actualTypeArguments.length > 0) {
                Type type = actualTypeArguments[0];

                // 这里需要检查type是否确实是Class<?>类型，因为实际类型参数可能是其他类型，如泛型表达式
                if (type instanceof Class<?>) {
                    System.out.println((Class<User>) type);
                }
            }
        }
    }

    @Test
    void demo03() throws IllegalAccessException {
        List<User> list = new ArrayList<>();
        list.add(new User(1,"张三",1,""));
        list.add(new User(2,"李四",1,""));
        list.add(new User(3,"王五",1,""));
        List<Object> user1 = elasticsearchUtil.queryHighLight("w", new User().getClass(), "user", client);
        System.out.println(user1);
    }

    @Test
    void demo04(){
        YxhEsServiceImpl<String> stringService = new YxhEsServiceImpl<>();

        // 获取泛型类型参数T的Class对象
        Class<String> typeParameterClass = getTypeParameterClass((Class<? extends YxhEsServiceImpl<?>>) stringService.getClass());

        // 输出泛型类型参数T的名称
        System.out.println("泛型类型参数T的名称: " + typeParameterClass.getName()); // 输出: java.lang.String
    }

    public static <T> String getGenericTypeName(T instance) {
        // 获取实例的类
        Class<?> clazz = instance.getClass();

        // 获取类的泛型父类信息
        Type genericSuperclass = clazz.getGenericSuperclass();

        if (genericSuperclass instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
            // 获取实际的泛型参数类型
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();

            // 假设 YxhEsServiceImpl 只接受一个泛型参数
            if (actualTypeArguments.length > 0) {
                return actualTypeArguments[0].getTypeName();
            }
        }
        return null;
    }

//    public static <T> Class<T> getTypeParameterClass(Class<? extends YxhEsServiceImpl<?>> clazz) {
//        // 获取父类的泛型类型信息（这里的父类是YxhEsServiceImpl<T>，但由于clazz是YxhEsServiceImpl的子类（或自身），所以这是有效的）
//        Type genericSuperclass = clazz.getGenericSuperclass();
//
//        if (genericSuperclass instanceof ParameterizedType) {
//            ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
//            // 获取实际的泛型参数类型
//            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
//
//            // 假设YxhEsServiceImpl只接受一个泛型参数，并且我们想要获取这个参数的类型
//            if (actualTypeArguments.length > 0) {
//                // 将Type转换为Class（这里可能会抛出ClassCastException，如果实际类型参数不是Class类型的话）
//                // 注意：如果泛型参数是泛型类型（比如List<String>），这里将只会返回List.class，而不是具体的参数化类型
//                return (Class<T>) actualTypeArguments[0];
//            }
//        }
//        throw new IllegalArgumentException("Class " + clazz.getName() + " is not parameterized with at least one type argument");
//    }

    @Test
    void demo50(){
        Class<?> interfaceT = getClassT(new YxhEsServiceImpl<>(), 0);
        System.out.println(interfaceT);
    }

    @Test
    void demo51(){
        Class<?> interfaceT = checkType(ServiceImpl.class, 0);
        System.out.println(interfaceT);
    }

    /**
     * 获取接口上的泛型T
     *
     * @param o     接口
     * @param index 泛型索引
     */
    public static Class<?> getInterfaceT(Object o, int index) {
        Type[] types = o.getClass().getGenericInterfaces();
        ParameterizedType parameterizedType = (ParameterizedType) types[index];
        Type type = parameterizedType.getActualTypeArguments()[index];
        return checkType(type, index);

    }


    /**
     * 获取类上的泛型T
     *
     * @param o     接口
     * @param index 泛型索引
     */
    public static Class<?> getClassT(Object o, int index) {
        Type type = o.getClass().getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type actType = parameterizedType.getActualTypeArguments()[index];
            return checkType(actType, index);
        } else {
            String className = type == null ? "null" : type.getClass().getName();
            throw new IllegalArgumentException("Expected a Class, ParameterizedType"
                    + ", but <" + type + "> is of type " + className);
        }
    }

    private static Class<?> checkType(Type type, int index) {
        if (type instanceof Class<?>) {
            return (Class<?>) type;
        } else if (type instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) type;
            Type t = pt.getActualTypeArguments()[index];
            return checkType(t, index);
        } else {
            String className = type == null ? "null" : type.getClass().getName();
            throw new IllegalArgumentException("Expected a Class, ParameterizedType"
                    + ", but <" + type + "> is of type " + className);
        }
    }

    private Class<?> deSerializable() {
        Type type = getClass().getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            System.out.println(parameterizedType.getActualTypeArguments()[0]);
            return (Class<?>) parameterizedType.getActualTypeArguments()[0];
        }
        throw new RuntimeException();
    }

    @Test
    void demo52(){
        deSerializable();
    }

    @Test
    void demo53(){

    }



}
