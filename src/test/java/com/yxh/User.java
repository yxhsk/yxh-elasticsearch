package com.yxh;

import com.yxh.annotions.DocumentIndex;
import com.yxh.annotions.HighLight;
import com.yxh.annotions.IdIndex;
import com.yxh.annotions.PropertiesIndex;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author：易小航
 * @name：User
 * @Date：2024/9/14 18:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@DocumentIndex(indexName = "user")
public class User {
    @IdIndex
    private Integer userId;
    @HighLight(mappingField = "highLightContent")
    private String userName;//张三
    @PropertiesIndex(name = "clazz",type = "long")
    private Integer clazzId;
    private String highLightContent;//<span>张三</span>
}
